require 'savon'
require 'tww/version'
require 'tww/configuration'
require 'tww/unimessage'

module Tww
  class << self
    attr_accessor :configuration
  end

  def self.configure
    @configuration ||= Configuration.new
    yield(configuration)
  end
end
