require 'spec_helper'

module Tww
  describe Configuration do
    describe "#username" do
      it 'does register username' do
        config = Configuration.new
        config.username = 'FakerUsername'

        expect(config.username).to eq('FakerUsername')
      end
    end

    describe "#password" do
      it 'does register password' do
        config = Configuration.new
        config.password = 'FakerPassword'

        expect(config.password).to eq('FakerPassword')
      end
    end
  end
end
